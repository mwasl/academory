package components;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import logicui.Window;


public class ButtonNewGameListener implements ActionListener {

    private Window window;

    public ButtonNewGameListener(Window window) {
        this.window = window;
    }

    public void actionPerformed(ActionEvent ae) {

        for (Kachel kachel: window.getAcademory().getkachelListe()) {
            kachel.setVisible(true);
        }

        new TimerListener(window.getLBLTimer());
        window.getNewGameJButton().setVisible(false);
        window.getAboutJButton().setBounds(10,40,100,30);

        window.getLBLTimer().setVisible(true);
        window.getLBLAnzeigeNamen().setVisible(true);
        window.getLBLAcademory().setVisible(false);
        window.getAWLogo().setVisible(false);

        window.getLBLScore().setVisible(true);
        window.getLBLScore().setText("Score: 0");

        window.getLBLPicks().setVisible(true);
        window.getLBLPicks().setText("Picks:  0");
    }

}
