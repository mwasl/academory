package components;

import javax.swing.Timer;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TimerListener extends Timer implements ActionListener {

    private JLabel lblTimer;
    private int seconds = 0;


    public TimerListener(JLabel lblTimer) {

        super(1000, null);
        addActionListener(this);

        this.lblTimer = lblTimer;
        this.start();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        this.lblTimer.setText("Timer: " + seconds++);
    }

}
