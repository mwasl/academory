package components;

import logicui.Academory;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;


public class Kachel extends JLabel implements MouseListener {

    private static int anzahlErstellterKacheln;
    private Academory academory;
    private final int KACHELBREITE = 100;
    private int id, nrRandom;
    private int x = 10, y = 40;
    private ImageIcon comparedImage;
    private ImageIcon tempImage;
    private String person;
    private String vorName;
    private boolean isCompared = false;
    private boolean changeColor = true;
    private final int ABSTAND = 5;
    private int offsetY = 105;
    private int columns = 7;

    public Kachel(int nrRandom, String person, Academory academory) {

        this.id = ++anzahlErstellterKacheln;
        this.nrRandom = nrRandom;
        this.person = person;
        this.academory = academory;
        this.vorName = person.split(" ")[0];

        this.setVisible(false);
        this.setBorder(BorderFactory.createLineBorder(Color.black, 3, false));
        this.setIcon(getRandomColorImage());
        this.setText(String.valueOf(this.id));

        this.setHorizontalTextPosition(JLabel.CENTER);
        this.setVerticalTextPosition(JLabel.CENTER);
        this.setHorizontalAlignment(SwingConstants.CENTER);

        this.addMouseListener(this);

        if (this.id < columns) { this.x += id * KACHELBREITE + id * ABSTAND; }
        else if (id < (columns * 2)) { setXandY(1); }
        else if (id < (columns * 3)) { setXandY(2); }
        else if (id < (columns * 4)) { setXandY(3); }
        else if (id < (columns * 5)) { setXandY(4); }
        else if (id < (columns * 6)) { setXandY(5); }
        else if (id < (columns * 7)) { setXandY(6); }
        super.setBounds(x, y, KACHELBREITE, KACHELBREITE);
    }

    private void setXandY(int row) {
        this.y += (offsetY * row);
        this.x += (this.id - (columns * row)) * KACHELBREITE + (this.id - (columns * row)) * ABSTAND;
    }

    private ImageIcon getRandomColorImage() {
        Random rand = new Random();
        int colorNr = rand.nextInt(7) + 1;
        ImageIcon colorImage = null;

        switch (colorNr) {
            case 1: colorImage = new ImageIcon("res/colors/yellow.png"); break;
            case 2: colorImage = new ImageIcon("res/colors/orange.png"); break;
            case 3: colorImage = new ImageIcon("res/colors/blue.png"); break;
            case 4: colorImage = new ImageIcon("res/colors/green.png"); break;
            case 5: colorImage = new ImageIcon("res/colors/lightblue.png"); break;
            case 6: colorImage = new ImageIcon("res/colors/red.png"); break;
            case 7: colorImage = new ImageIcon("res/colors/purple.png"); break;
        }
        return colorImage;
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        if (academory.are2KachelnNotNull()) {

            academory.resetComparedKacheln();
            academory.getWindow().setAnzeigeNamen(null);
        } else {
            if (!isCompared) {
                academory.getWindow().setAnzeigeNamen(this.person);
                this.setIcon(new ImageIcon("res/persons/" + vorName + ".png"));
                this.setText(null);
                academory.setAndCompareKacheln(this);
            }
        }
    }

    @Override
    public void mouseEntered (MouseEvent mouseEvent){

        if (!(isCompared)) {
            this.tempImage = (ImageIcon) getIcon();
            this.comparedImage = (ImageIcon) getIcon();
            this.setIcon(new ImageIcon("res/colors/greenselect.png"));
        }
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void mouseExited (MouseEvent mouseEvent){

        if (!(isCompared) && changeColor) {
            this.setIcon(tempImage);
            this.setIcon(comparedImage);
        }
        changeColor = true;
        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    public String toString () {
        return "id: " + this.id + ", nrRandom: " + this.nrRandom + ", name: " + this.person;
    }

    public int getX () {
        return this.x;
    }

    public int getY () {
        return this.y;
    }

    public int getId () {
        return this.id;
    }

    public int getKACHELBREITE () {
        return this.KACHELBREITE;
    }

    public void setIsCompared ( boolean bool){
        this.isCompared = bool;
    }

    public String getPerson () {
        return this.person;
    }

    public Icon getComparedImage () {
        return this.comparedImage;
    }

    public int getNrRandom () { //drinlassen zu ggf. Prüfungszwecken in Academory, Zeile ca. 88
        return this.nrRandom;
    }

    @Override
    public void mouseReleased (MouseEvent mouseEvent){
    }

    @Override
    public void mouseClicked (MouseEvent mouseEvent){
    }

}



