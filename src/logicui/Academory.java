package logicui;

import components.Kachel;
import javax.swing.*;
import java.util.*;


public class Academory {

    private Window window;
    private List<Kachel> kachelListe;
    private final String[] NAMEN = {"Robert Bellin", "Kerstin Bonev-Wagner", "Christoph Döhnel", "Jochen Erich Embruch", "Philip Frank", "Linus Hasler ", "Alexander Hoffman", "Anabel Huba", "Tim Jacobsen", "Ida Jaganjac", "Daniel Knogler", "Adam Korkosz", "David Léger", "Philipp Leipold", "Sonja Offermann", "Sophie Presuhn", "Thomas Raich", "Andreas Redeker", "Lucas Rudolf ", "Maurice Schenk ", "Lisa Schmidt-Berschet", "Kira Schulz", "Mona Wegscheider", "Lukas Ziebell"};
    private List<String> namenDoppelt;
    private List<Integer> nummernQuelle;
    private List<Integer> nummernListeRandom;
    private Kachel comparedKachel1;
    private Kachel comparedKachel2;
    private int score = 0;
    private static int picks;

    public Academory() {

        this.namenDoppelt = new ArrayList();
        this.nummernQuelle = new ArrayList();
        this.nummernListeRandom = new ArrayList();
        this.kachelListe = new ArrayList();
        this.createKachelFelder();

        window = new Window(this);
        SwingUtilities.invokeLater(window);
    }

    public void createKachelFelder(){

        this.namenDoppelt.addAll(Arrays.asList(NAMEN));
        this.namenDoppelt.addAll(Arrays.asList(NAMEN));
//        Collections.shuffle(namenDoppelt);

        Random rand = new Random();

        for (int i = 1; i <= namenDoppelt.size(); i++) {            //Array mit Nummern 1 - 49 erstellen
            nummernQuelle.add(i);
        }

        while (nummernListeRandom.size() < namenDoppelt.size()) {   // random Reihenfolge reinbringen
            int numberFound = rand.nextInt(nummernQuelle.size());
            if (!(nummernListeRandom.contains(numberFound))) {
                nummernListeRandom.add(nummernQuelle.get(numberFound));
                nummernQuelle.remove(numberFound);
            }
        }

        for (int i: nummernListeRandom) {
            Kachel kachel = new Kachel(i, namenDoppelt.get(i-1),this);
            kachelListe.add(kachel);
        }
    }

    public void updatePicksCount(){
        window.setPicks(++picks);
    }

    public void setAndCompareKacheln(Kachel kachel) {

        if (comparedKachel1 == null) {
            comparedKachel1 = kachel;
            comparedKachel1.setIsCompared(true);
            updatePicksCount();
            return;
        }

        if (comparedKachel2 == null) {
            comparedKachel2 = kachel;
            comparedKachel2.setIsCompared(true);
            updatePicksCount();
        }

        if (found2EqualTiles()) {
            comparedKachel1.setVisible(false);
            comparedKachel2.setVisible(false);
            this.increaseScore();
            window.setLBLScore(this.getScore());
        }
    }

    public boolean found2EqualTiles() {
        return (comparedKachel1.getPerson().equals(comparedKachel2.getPerson()) && (comparedKachel1.getId() != comparedKachel2.getId()));
    }

    public boolean are2KachelnNotNull() {
        return comparedKachel1 != null && comparedKachel2 != null;
    }

    public void resetComparedKacheln() {

        this.comparedKachel1.setIcon(new ImageIcon());
        this.comparedKachel2.setIcon(new ImageIcon());
        this.comparedKachel1.setIsCompared(false);
        this.comparedKachel2.setIsCompared(false);
        this.comparedKachel1.setIcon(comparedKachel1.getComparedImage());
        this.comparedKachel2.setIcon(comparedKachel2.getComparedImage());
        this.comparedKachel1.setText(String.valueOf(comparedKachel1.getId()));
        this.comparedKachel2.setText(String.valueOf(comparedKachel2.getId()));
        this.comparedKachel1 = null;
        this.comparedKachel2 = null;
    }

    public void increaseScore() {
        float multi = 1;
        if      (picks <= 36)  { multi = 3.5F; }
        else if (picks <= 62)  { multi = 3.0F; }
        else if (picks <= 84)  { multi = 2.5F; }
        else if (picks <= 106) { multi = 2.0F; }
        else if (picks <= 130) { multi = 1.5F; }
        else { multi = 1.0F; }

        score += 100 * multi;
    }

    public List<Kachel> getkachelListe() {
        return this.kachelListe;
    }

    public Window getWindow() {
        return this.window;
    }

    public int getScore() {
        return this.score;
    }

}
