package logicui;

import components.ButtonNewGameListener;
import components.Kachel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window implements Runnable{

    private JFrame frame;
    private Academory academory;
    private JLabel lblAWLogo;
    private JLabel lblScore;
    private JLabel lblPicks;
    private JLabel lblAcademory;
    private JLabel lblAnzeigeNamen;
    private JLabel lblTimer;
    private JButton butNewGame;
    private JButton butAbout;


    public Window(Academory academory) {
        this.frame = new JFrame("Academory");
        this.academory = academory;
    }

    @Override
    public void run() {

        frame.setPreferredSize(new Dimension(770, 820));
        frame.setResizable(false);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setIconImage(new ImageIcon("res/aw_logo_small.png").getImage());

        createComponents(frame.getContentPane());
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void createComponents(Container cont) {

        lblAnzeigeNamen = returnJLabel("Let's get started, pick a tile.",0,0,770,40,28,false);
        lblAnzeigeNamen.setHorizontalAlignment(SwingConstants.CENTER);

        lblTimer = returnJLabel("Timer: 0", 10,77,100,30,16,true);
        lblScore = returnJLabel("Score: 0", 10,97,100,30,16,false);
        lblPicks = returnJLabel("Picks:  0",10,117,100,30,16,false);
        lblAcademory = returnJLabel("Academory", 90, 130, 630, 150,110, true);
        lblAcademory.setForeground(new Color(17, 122, 101));

        ImageIcon img = new ImageIcon("res/aw_academy_logo.png");
        lblAWLogo = new JLabel(img);
        lblAWLogo.setBounds(190, 250, 400, 170);

        JLabel lblBackGround = new JLabel(new ImageIcon("res/background.png"));
        lblBackGround.setLayout(null);

        butNewGame = returnJButton("New Game",10,40,100,30,false);
        butNewGame.addActionListener(new ButtonNewGameListener(this));

        butAbout = returnJButton("About",10,70,100,30,false);

        ActionListener al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                ImageIcon img = new ImageIcon("res/persons/Adam.png");
                JOptionPane oPane = new JOptionPane();
                JLabel labelMessage = new JLabel("<HTML><strong><p style=\"font-size:18px\">Adam Korkosz</p></strong><br>Java Consultant der Academy Winter 2019<br><br><br>Mit dieser Memory App lernen wir alle Academy Teilnehmer spielend leicht kennen.<br><br>Meine Interessen:<br><br>Java <br>VBA <br>SAP <br>Logistik <br>CPU + GPU");
                labelMessage.setFont(new Font("Calibri",Font.PLAIN,16));
                oPane.showMessageDialog(null, labelMessage,"Information über den Entwickler:", JOptionPane.INFORMATION_MESSAGE, img);
            }
        };
        butAbout.addActionListener(al);

        frame.setLayout(null);
        frame.setContentPane(lblBackGround);

        addComponentsToFrame(new JComponent[]{butNewGame, butAbout, lblTimer, lblScore, lblAcademory, lblPicks, lblAWLogo, lblAnzeigeNamen});
        createKachelnAddToFrame();
    }

    private JButton returnJButton(String text, int x, int y, int width, int heigth, boolean focusBool) {
        JButton returnButton = new JButton(text);
        returnButton.setBounds(x, y, width, heigth);
        returnButton.setFocusable(focusBool);
        returnButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return returnButton;
    }

    private JLabel returnJLabel(String text, int x, int y, int width, int height, int fontSize, boolean visiBool) {
        JLabel returnLabel = new JLabel(text);
        returnLabel.setBounds(x, y, width,height);
        returnLabel.setFont(new Font(null, Font.BOLD, fontSize));
        returnLabel.setVisible(visiBool);
        return returnLabel;
    }

    private void addComponentsToFrame(JComponent[] components) {
        for (JComponent comp: components) {
            frame.add(comp);
        }
    }

    private void createKachelnAddToFrame() {
        for (Kachel kachel: academory.getkachelListe()) {
            kachel.setBounds(kachel.getX(), kachel.getY(), kachel.getKACHELBREITE(), kachel.getKACHELBREITE());
            frame.add(kachel);
        }
    }

    public JLabel getAWLogo() {
        return this.lblAWLogo;
    }

    public JLabel getLBLScore() {
        return this.lblScore;
    }

    public JLabel getLBLPicks() {
        return this.lblPicks;
    }

    public JLabel getLBLAnzeigeNamen() {
        return this.lblAnzeigeNamen;
    }

    public JLabel getLBLAcademory() {
        return this.lblAcademory;
    }

    public JLabel getLBLTimer() {
        return this.lblTimer;
    }

    public JButton getNewGameJButton() {
        return this.butNewGame;
    }

    public JButton getAboutJButton() {
        return this.butAbout;
    }

    public void setLBLScore(int score) {
        this.lblScore.setText("Score: " + score);
    }

    public void setPicks(int picks) {
        this.lblPicks.setText("Picks:  " + picks);
    }


    public void setAnzeigeNamen(String name) {
        this.lblAnzeigeNamen.setText(name);
    }

    public Academory getAcademory() {
        return this.academory;
    }
}
